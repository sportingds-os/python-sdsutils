import setuptools as sut

sut.setup(
    name="sdsutils",
    version="0.1.0",
    packages=sut.find_packages(),
    author="DaveHastie",
    author_email="dave.hastie@sportingds.com",
    description="Sporting Data Science open source utilities",
    license="FILE",
    include_package_data=True
)

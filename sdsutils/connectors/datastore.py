import sdsutils.connectors.mysql as mysql
import sdsutils.connectors.mongo as mongo


class DataStore:
    def __init__(self, data_stores=[]):
        for ds in data_stores:
            self.add_data_store(ds)

    def add_data_store(self, ds):
        self.__setattr__(ds, None)

    def connect(self, config=None, config_files=None, package=None, environment=None, read_preference=None,
                db_names=None):
        if hasattr(self, 'mysql'):
            self.mysql = mysql.MySQLConnection(config=config, config_files=config_files, package=package,
                                               environment=environment,db_names=db_names)
        if hasattr(self, 'mongo'):
            self.mongo = mongo.MongoConnection(config=config, config_files=config_files, package=package,
                                               environment=environment, db_names=db_names,
                                               read_preference=read_preference)

import warnings
import sqlalchemy as sqla
import sqlalchemy.ext.declarative as dec

import sdsutils.confighandlers.toml as toml
import sdsutils.random.random_string as rnd_str


Base = dec.declarative_base(cls=dec.DeferredReflection)


class MySQLConnection:

    def __init__(self, config=None, config_files=None, package=None, environment=None, db_names=None):
        self.eng = {}
        self.config = config if config is not None else {}
        self.test_db_name = None
        if config_files or package:
            self.connect_from_file(config_files, package, environment, db_names)

    def connect_from_file(self, config_files, package, environment, db_names):
        toml.TomlConfig(self.config, config_files, package=package, environment=environment, sections=['mysql'])
        if 'mysql' not in self.config:
            # Raise an exception here not a warning
            warnings.warn("No 'mysql' section in config files provided. No database connection.")
        for k, v in self.config['mysql'].items():
            if db_names and k not in db_names:
                continue
            self.eng[k] = sqla.create_engine(v, pool_recycle=3600)

    def connect_by_name(self, db_name):
        if 'mysql' not in self.config:
            # Raise an exception here not a warning
            warnings.warn("No 'mysql' section in config property of MySQLConnection instance. No database connection.")
        else:
            if 'server' not in self.config['mysql']:
                warnings.warn("No server url in MySQLConnection instance. No database connection.")
            else:
                db_url = self.config['mysql']['server'] + '/' + db_name['full']
                self.eng[db_name['short']] = sqla.create_engine(db_url, pool_recycle=3600)

    def create_db_by_name(self, db_name):
        if 'server' not in self.eng:
            warnings.warn("No server url in MySQLConnection instance. Cannot create database.")
        else:
            self.eng['server'].execute('CREATE DATABASE IF NOT EXISTS ' + db_name + ';')

    def add_test_db(self):
        self.test_db_name = 'test_' + rnd_str.generate()
        self.create_db_by_name(self.test_db_name)
        self.connect_by_name({'short': 'test', 'full': self.test_db_name})

    def remove_test_db(self):
        if 'test' in self.eng:
            self.drop_db_by_name(self.test_db_name)
            del self.eng['test']

    def drop_db_by_name(self, db_name):
        if 'server' not in self.eng:
            warnings.warn("No server url in MySQLConnection instance. Cannot drop database.")
        else:
            self.eng['server'].execute('DROP DATABASE IF EXISTS ' + db_name + ';')


def create_test_db_connection(config_files=None, package=None):
    conn = MySQLConnection(config_files=config_files, package=package, db_names=['server'])
    conn.add_test_db()

    return conn

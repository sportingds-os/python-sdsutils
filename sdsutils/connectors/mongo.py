import pymongo
import warnings
import copy
import datetime as dt
import pytz

import sdsutils.confighandlers.toml as toml


class MongoConnection:

    def __init__(self, config=None, config_files=None, package=None, environment=None, db_names=None,
                 read_preference=None):
        self.client = None
        self.dbs = {}
        self.config = config if config is not None else {}
        self.connect_from_file(config_files=config_files, db_names=db_names, package=package, environment=environment,
                               read_preference=read_preference)

    def connect_from_file(self, config_files, db_names=None, package=None, environment=None, read_preference=None):
        toml.TomlConfig(self.config, config_files=config_files, package=package, environment=environment,
                        sections=['mongo'])
        if self.config is None or 'mongo' not in self.config:
            # Raise an exception here not a warning
            warnings.warn("No 'mongo' section in config files provided. No database connection.")

        rp = read_preference if read_preference is not None else pymongo.ReadPreference.PRIMARY_PREFERRED
        self.client = pymongo.MongoClient(self.config['mongo']['uri'], read_preference=rp)
        available_dbs = self.client.database_names()
        for db in available_dbs:
            if db_names is not None and db not in db_names:
                continue
            self.dbs[db] = pymongo.database.Database(self.client, db)


class MongoBase:
    """Base class for Mongo Objects. Intended to be immutable."""
    def __init__(self, collection, identifier_key, doc=None, query=None, projection=None):
        self.__collection = collection
        self.__id_key = identifier_key
        if doc is not None:
            self.__document = copy.deepcopy(doc)
        elif query is not None:
            self.__document = self.__collection.find_one(query, projection)
        else:
            raise ValueError('MongoBase must be initialised with a document or query')

        def protected_setattr(slf, key, value):
            raise AttributeError('MongoBase class is immutable')
        self.__setattr__ = protected_setattr

    def __eq__(self, other):
        if isinstance(other, MongoBase):
            for k in self.__id_key:
                if self.unflatten(self.__document, k) != self.unflatten(other.__document, k):
                    return False
            return True
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def __hash__(self):
        return hash((self.__document[k] for k in self.__id_key))

    def doc(self):
        return self.__document

    def write_to_db(self):

        filt = {}
        for k in self.__id_key:
            filt[k] = self.unflatten(self.__document, k)

        upd = self.flatten({'$set': {}, '$unset': {}}, '', self.__collection.find_one(filt, {'_id': -1}))
        for k, v in self.__document.items():
            if k == '_id':
                continue
            upd = self.flatten(upd, k, v)

        if not upd['$set']:
            del upd['$set']
        if not upd['$unset']:
            del upd['$unset']

        inserted_id = None
        if upd:
            if '$set' in upd:
                upd['$set']['update_dt'] = {'gmt': dt.datetime.now(tz=pytz.utc)}
            else:
                upd['$set'] = {'update_dt': {'gmt': dt.datetime.now(tz=pytz.utc)}}
            res = self.__collection.find_one_and_update(filt, upd, projection={'_id': 1}, upsert=True,
                                                        return_document=pymongo.ReturnDocument.AFTER)
            inserted_id = res['_id']

        return inserted_id

    def flatten(self, update, flat_name, val):

        if isinstance(val, dict):
            if val:
                for k, v in val.items():
                    k1 = k
                    if len(flat_name) > 0:
                        k1 = flat_name + '.' + k
                    update = self.flatten(update, k1, v)
        else:
            if type(val) is RemoveType:
                update['$unset'][flat_name] = 1
            else:
                if val is not None:
                    if not isinstance(val, list) or len(val) > 0:
                        update['$set'][flat_name] = val

        return update

    def unflatten(self, d, k):
        if '.' in k:
            k.split('.')
            out = self.unflatten(d[k.split('.')[0]], '.'.join(k.split('.')[1:]))
        else:
            out = d[k]
        return out


class RemoveType:
    def __bool__(self):
        return False

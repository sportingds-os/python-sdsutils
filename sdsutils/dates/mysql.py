import datetime as dt


def datetime_from_mysql(dt_str):

    if dt_str is None:
        return None

    tmp = dt_str
    if isinstance(tmp, str):
        tmp = dt.datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S')

    return tmp.strftime('%Y-%m-%dT%H:%M:%SZ%z')


def date_from_mysql(dt_str):

    if dt_str is None:
        return None

    tmp = dt_str
    if isinstance(tmp, str):
        tmp = dt.datetime.strptime(dt_str, '%Y-%m-%d %H:%M:%S')

    return tmp.strftime('%Y-%m-%d')

def utc_offset_str(utc_offset):
    offset_mins = 60 * (utc_offset % 1)
    offset_str = ('-' if utc_offset < 0 else '+') + ('0' if utc_offset > -10 and utc_offset < 10 else '') + \
                 str(int(utc_offset)) + ':' + ('0' if offset_mins < 10 else '') + str(offset_mins)
    return offset_str

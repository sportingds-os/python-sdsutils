from os.path import join, realpath, dirname

CONFIG_DIR = join(dirname(dirname(realpath(__file__))), 'instance', 'config')
CONFIG_FILES = [join(CONFIG_DIR, f) for f in ['local.toml']]

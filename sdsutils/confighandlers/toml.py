import toml
import warnings
import copy

import sdsutils.confighandlers.config_locator as config_locator


class TomlConfig:

    def __init__(self, config=None, config_files=None, sections=None, package=None, environment=None):
        # The config files should be specified in order, with the files later in the list overriding those earlier.
        # Config items supplied in the config argument override what is in the files
        # This next line ensures that we are updating the config argument passed in (important for Flask where the
        # app.config is passed in)
        self.config = config
        safe_config = copy.deepcopy(config)

        self.read_config(config_files, package, environment, sections)
        # This is the overwrite of the config argument over what was read in.
        if safe_config is not None and isinstance(safe_config, dict):
            self.config = self.update_config(self.config, safe_config)

    def read_config(self, config_files=None, package=None, environment=None, sections=None):

        if package is None:
            package = 'sdsutils'

        if not config_files or len(config_files) == 0:
            conf_locator = config_locator.ConfigLocator(package, environment)
            config_files = conf_locator.config_files

        for conf_file in config_files:
            try:
                with open(conf_file) as cf:
                    config = toml.loads(cf.read())
                    new_config = {}
                    for k, v in config.items():
                        if not sections or k in sections:
                            new_config[k] = v
                self.config = self.update_config(self.config, new_config)
            except FileNotFoundError:
                warnings.warn(conf_file + " not found. Intended config not processed.")

    def update_config(self, existing_config, new_config):
        out = existing_config

        if not isinstance(existing_config, dict):
            out = new_config
            return out

        for k, v in new_config.items():
            if not k in out.keys():
                out[k] = v
            else:
                if isinstance(v, dict):
                    out[k] = self.update_config(out[k], v)
                else:
                    out[k] = v
        return out

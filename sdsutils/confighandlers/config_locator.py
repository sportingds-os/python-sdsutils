from os.path import join, isdir, expanduser
import os


class ConfigLocator:

    def __init__(self, package, environment=None):
        self._config_dir = join(expanduser('~'), 'sdsutils/config', package)
        if not isdir(self._config_dir) or not os.access(self._config_dir, os.R_OK):
            self._config_dir = join('/etc/sdsutils/config', package)
        assert isdir(self._config_dir)
        assert os.access(self._config_dir, os.R_OK)

        file_parts = ['local']
        if environment is not None:
            file_parts.append('_' + environment)
        file_parts.append('.toml')
        file_name = "".join(file_parts)

        self.__config_files = [join(self._config_dir, file_name)]

    @property
    def config_files(self):
        return self.__config_files

    @config_files.setter
    def config_files(self, cfs):
        if isinstance(cfs, (list, tuple)):
            self.__config_files = cfs
        else:
            self.__config_files = [cfs]

    def add_config_file(self, cf):
        self.__config_files.append(cf)

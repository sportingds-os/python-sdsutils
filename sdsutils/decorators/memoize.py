def memoize(f):
    memo = {}

    def helper(x):
        if x not in memo:
            memo[x] = f(x)
        return memo[x]
    return helper


def memoize2(f):
    memo = {}

    def helper(x, y):
        if y not in memo:
            memo[y] = f(x, y)
        return memo[y]
    return helper
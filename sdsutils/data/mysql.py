def unpack_results(res, res_map, remove_nulls=True):

    data = []
    for row in res:
        tmp = {}
        for item in res_map:
            mysql_key = 'mysql_key' if 'mysql_key' in item and item['mysql_key'] else 'key'
            if 'f' in item and item['f']:
                f = item['f']
                arg_list = [row[item[mysql_key]]] if not isinstance(item[mysql_key], list) else \
                    [row[i] for i in item[mysql_key]]
                create_sub_dicts(item['key'], f(*arg_list), tmp)
            else:
                create_sub_dicts(item['key'], row[item[mysql_key]], tmp)
        if remove_nulls:
            remove_mysql_nulls(tmp)
        if tmp:
            data.append(tmp)
    return data


def create_sub_dicts(key_str, value, row_dict):
    substrings = key_str.split('.')
    tmp = row_dict
    for i, s in enumerate(substrings):
        if i == (len(substrings) - 1):
            tmp[s] = value
        else:
            if s not in tmp:
                tmp[s] = {}
            tmp = tmp[s]


def remove_mysql_nulls(d):
    indic = False
    # Need to define keys separately so not changing object we are iterating over
    keys = list(d.keys())
    for k in keys:
        if isinstance(d[k], dict):
            indic = remove_mysql_nulls(d[k])
            if not indic:
                d.pop(k)
        else:
            if d[k] is None:
                d.pop(k)
            else:
                indic = True
    return indic

DELIMITER $$
CREATE DEFINER=`davehastie`@`%` PROCEDURE `cricket_v3_source`.`opta_team_upsert`(
 {0}
)
    SQL SECURITY INVOKER
BEGIN

	  {1}

    DECLARE p_force TINYINT DEFAULT 0;
    IF i_force IS NOT NULL THEN
		  SET p_force = i_force;
	  END IF;

    SELECT {2}.version, {2}.current, {3} INTO
    p_version, p_current, {4}
    FROM {5} AS {2}
    WHERE {6}
    AND {2}.version = (SELECT MAX(version) FROM {5} WHERE {6});

    IF p_version IS NULL THEN
		  START TRANSACTION;
		  INSERT INTO {5}(version, {7})
			  VALUES(1, {8});
		  {9}
		  COMMIT;
    ELSE
		  IF p_current = 0 OR p_force = 1 {10} THEN
			  START TRANSACTION;
        INSERT INTO {5}(version, {7})
				  VALUES(p_version + 1, {11});
			  UPDATE {5} SET current=0 WHERE {6} AND version <= p_version AND current=1;
			  COMMIT;
		  END IF;
    END IF;

    SELECT {2}.version, {2}.current, {2}.deleted, {3}
    FROM {5} AS {2}
    WHERE {6}
    AND {2}.version = (SELECT MAX(version) FROM {5} WHERE {6});

END$$
DELIMITER ;
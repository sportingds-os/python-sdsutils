def int_or_none(x):
    if x is not None:
        x = int(x)
    return x


def is_positive_int(x):
    return int(x) > 0

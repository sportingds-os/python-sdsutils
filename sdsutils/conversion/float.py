def float_or_none(x):
    if x is not None:
        x = float(x)
    return x

# Functions to create package skeletons

# Flask
#  root
#  - app
#    - __init__.py
#    - controllers
#      - __init__.py
#    - static
#      - docs
#       - css [ONLY IF NOT API]
#       - js [ONLY IF NOT API]
#       - vendor
#    - templates  [ONLY IF NOT API]
#      - __init__.py
#      - base.html
#  - instance
#    - config
#      - settings.toml
#      - local.toml
#  - .gitignore
#  - requirements.txt
#  - run.py



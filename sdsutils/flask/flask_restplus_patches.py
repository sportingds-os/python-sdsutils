from six import iteritems
from werkzeug.utils import cached_property

from flask_restplus.api import Api as OriginalApi
from flask_restplus.swagger import Swagger as OriginalSwagger, extract_path_params, parse_docstring
from flask_restplus.reqparse import Argument as OriginalArgument, RequestParser as OriginalRequestParser, \
    ParseResult, LOCATIONS, text_type, _handle_arg_type
from flask_restplus.model import Model
from flask_restplus.utils import merge, not_none
from flask_restplus._compat import OrderedDict


class Api(OriginalApi):
    @cached_property
    def __schema__(self):
        """
        The Swagger specifications/schema for this API

        :returns dict: the schema as a serializable dict
        """
        if not self._schema:
            self._schema = Swagger(self).as_dict()
        return self._schema


class Swagger(OriginalSwagger):

    def extract_resource_doc(self, resource, url):
        doc = getattr(resource, '__apidoc__', {})
        if doc is False:
            return False
        doc['name'] = resource.__name__
        tmp_params = doc.get('params', OrderedDict())
        params = merge(self.expected_params(doc), tmp_params)
        params = merge(params, extract_path_params(url))
        doc['params'] = params
        for method in [m.lower() for m in resource.methods or []]:
            method_doc = doc.get(method, OrderedDict())
            method_impl = getattr(resource, method)
            if hasattr(method_impl, 'im_func'):
                method_impl = method_impl.im_func
            elif hasattr(method_impl, '__func__'):
                method_impl = method_impl.__func__
            method_doc = merge(method_doc, getattr(method_impl, '__apidoc__', OrderedDict()))
            if method_doc is not False:
                method_doc['docstring'] = parse_docstring(method_impl)
                method_params = self.expected_params(method_doc)
                tmp_params = method_doc.get('params', OrderedDict())
                tmp_params = OrderedDict(reversed(tmp_params.items()))
                method_params = merge(method_params, tmp_params)
                inherited_params = OrderedDict([(k, v) for k, v in iteritems(params) if k in method_params])
                method_doc['params'] = merge(inherited_params, method_params)
                p = OrderedDict([(k, v) for k, v in iteritems(params) if not k in method_params])
                doc['params'] = p

            doc[method] = method_doc
        return doc

    def expected_params(self, doc):
        params = OrderedDict()
        if 'expect' not in doc:
            return params

        for expect in doc.get('expect', []):
            if isinstance(expect, RequestParser):
                parser_params = OrderedDict([(p['name'], p) for p in expect.__schema__])
                params.update(parser_params)
            elif isinstance(expect, Model):
                params['payload'] = not_none({
                    'name': 'payload',
                    'required': True,
                    'in': 'body',
                    'schema': self.serialize_schema(expect),
                })
            elif isinstance(expect, (list, tuple)):
                if len(expect) == 2:
                    # this is (payload, description) shortcut
                    model, description = expect
                    params['payload'] = not_none({
                        'name': 'payload',
                        'required': True,
                        'in': 'body',
                        'schema': self.serialize_schema(model),
                        'description': description
                    })
                else:
                    params['payload'] = not_none({
                        'name': 'payload',
                        'required': True,
                        'in': 'body',
                        'schema': self.serialize_schema(expect),
                    })
        return params


class Argument(OriginalArgument):
    """
    :param name: Either a name or a list of option strings, e.g. foo or -f, --foo.
    :param description: A description for swagger parameter.
    :param default: The value produced if the argument is absent from the request.
    :param dest: The name of the attribute to be added to the object
        returned by :meth:`~reqparse.RequestParser.parse_args()`.
    :param bool required: Whether or not the argument may be omitted (optionals only).
    :param string action: The basic type of action to be taken when this argument
        is encountered in the request. Valid options are "store" and "append".
    :param bool ignore: Whether to ignore cases where the argument fails type conversion
    :param type: The type to which the request argument should be converted.
        If a type raises an exception, the message in the error will be returned in the response.
        Defaults to :class:`unicode` in python2 and :class:`str` in python3.
    :param location: The attributes of the :class:`flask.Request` object
        to source the arguments from (ex: headers, args, etc.), can be an
        iterator. The last item listed takes precedence in the result set.
    :param choices: A container of the allowable values for the argument.
    :param help: A brief description of the argument, returned in the
        response when the argument is invalid. May optionally contain
        an "{error_msg}" interpolation token, which will be replaced with
        the text of the error raised by the type converter.
    :param bool case_sensitive: Whether argument values in the request are
        case sensitive or not (this will convert all values to lowercase)
    :param bool store_missing: Whether the arguments default value should
        be stored if the argument is missing from the request.
    :param bool trim: If enabled, trims whitespace around the argument.
    :param bool nullable: If enabled, allows null value in argument.
    """

    def __init__(self, name, description=None, default=None, dest=None, required=False,
                 ignore=False, type=text_type, location=('json', 'values',),
                 choices=(), action='store', help=None, operators=('=',),
                 case_sensitive=True, store_missing=True, trim=False,
                 nullable=True):
        self.name = name
        self.description = description
        self.default = default
        self.dest = dest
        self.required = required
        self.ignore = ignore
        self.location = location
        self.type = type
        self.choices = choices
        self.action = action
        self.help = help
        self.case_sensitive = case_sensitive
        self.operators = operators
        self.store_missing = store_missing
        self.trim = trim
        self.nullable = nullable

    @property
    def __schema__(self):
        if self.location == 'cookie':
            return
        param = {
            'name': self.name,
            'in': LOCATIONS.get(self.location, 'query')
        }
        _handle_arg_type(self, param)
        if self.required:
            param['required'] = True
        if self.description:
            param['description'] = self.description
        else:
            if self.help:
                param['description'] = self.help
        if self.default:
            param['default'] = self.default
        if self.action == 'append':
            param['items'] = {'type': param['type']}
            param['type'] = 'array'
            param['collectionFormat'] = 'multi'
        if self.choices:
            param['enum'] = self.choices
            param['collectionFormat'] = 'multi'
        return param


class RequestParser(OriginalRequestParser):
    """
    Enables adding and parsing of multiple arguments in the context of a single request.
    Ex::

        from flask_restplus import RequestParser

        parser = RequestParser()
        parser.add_argument('foo')
        parser.add_argument('int_bar', type=int)
        args = parser.parse_args()

    :param bool trim: If enabled, trims whitespace on all arguments in this parser
    :param bool bundle_errors: If enabled, do not abort when first error occurs,
        return a dict with the name of the argument and the error message to be
        bundled and return all validation errors
    """

    def __init__(self, argument_class=Argument, result_class=ParseResult,
            trim=False, bundle_errors=False):
        self.args = []
        self.argument_class = argument_class
        self.result_class = result_class
        self.trim = trim
        self.bundle_errors = bundle_errors

    def add_argument(self, *args, **kwargs):
        """
        Adds an argument to be parsed.

        Accepts either a single instance of Argument or arguments to be passed
        into :class:`Argument`'s constructor.

        See :class:`Argument`'s constructor for documentation on the available options.
        """

        if len(args) == 1 and isinstance(args[0], self.argument_class):
            self.args.append(args[0])
        else:
            self.args.append(self.argument_class(*args, **kwargs))

        # Do not know what other argument classes are out there
        if self.trim and self.argument_class is Argument:
            # enable trim for appended element
            self.args[-1].trim = kwargs.get('trim', self.trim)

        return self

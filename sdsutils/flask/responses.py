import datetime as dt

from flask import jsonify, render_template


def doc_response(doc_name):
    from app import app
    return render_template('docs/swagger/index.html', doc_url=app.config['swagger']['host'] + doc_name)


def unauthorised_response():
    out = {'last_updated': dt.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ%z'), 'status': 'error',
           'error': 'Unauthorized'}
    return wrap_response(out, 401)


def bad_input_response(error):
    out = {'last_updated': dt.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ%z'), 'status': 'error',
           'error': error}
    return wrap_response(out, 400)


def success_response(data, key_name, headers=None, is_select_control=None):

    if data is None or len(data) == 0:
        return success_no_data_response(key_name, headers, is_select_control)

    return success_data_response(data, key_name, headers, is_select_control)


def success_data_response(data, key_name, headers, is_select_control):
    out = {"last_updated": dt.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ%z'), "status": "success"}
    if headers is not None:
        out['headers'] = headers
    if is_select_control is not None:
        out['options']= data if is_select_control else []
        if isinstance(key_name, list):
            for k in key_name:
                if k in data:
                    out[k] = [] if is_select_control else data[k]
                else:
                    if is_select_control:
                        out[k] = []
        else:
            out[key_name] = [] if is_select_control else data

    else:
        if isinstance(key_name, list):
            for k in key_name:
                if k in data:
                    out[k] = data[k]
        else:
            out[key_name] = data

    return wrap_response(out, 200)


def success_no_data_response(key_name, headers, is_select_control):
    out = {"last_updated": dt.datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ%z'), "status": "no data"}
    if isinstance(key_name, list):
        for k in key_name:
            out[k] = []
    else:
        out[key_name] = []

    if headers is not None:
        out['headers'] = []
    if is_select_control is not None:
        out['options'] = []

    return wrap_response(out, 204)


def wrap_response(res, code):
    return jsonify(res), code, {'Content-Type': 'application/json'}
